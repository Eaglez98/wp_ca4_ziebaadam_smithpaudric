<?php require_once ('Model.php'); ?>
<?php

use Rapid\Database;

class Member {

    private $id;
    private $username;
    private $pass;
    private $phone;
    private $email;
    private $passport_number;


    public function __construct($args) {

        if (!is_array($args)) {
            throw new Exception('Member constructor requires an associative array');
        }

        $this->setId($args['id'] ?? NULL);
        $this->setUserName($args['username'] ?? 'Untitled Member');
        $this->setPassword($args['pass'] ?? 'Untitled pass');
        $this->setPhone($args['phone'] ?? NULL);
        $this->setEmail($args['email'] ?? NULL);
        $this->setPassportNumber($args['passport_number'] ?? NULL);
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getUserName() {
        return $this->username;
    }

    public function getPassword() {
        return $this->pass;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassportNumber() {
        return $this->passport_number;
    }

    // Setters
    public function setId($id) {

        if ($id === NULL) {
            $this->id = NULL;
            return;
        }

        if (!Model::isValidId($id)) {
            throw new Exception('Invalid ID passed to setId for Member');
        }
        $this->id = $id;
    }

    public function setUserName($username) {
        if (!preg_match('/^[A-Za-z +]{3,55}$/i', $username)) {
            throw new Exception('Invalid username');
        }
        $this->username = $username;
    }

    public function setPassword($pass) {
        // if (!preg_match('/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/i', $pass)) {
        //     throw new Exception('Invalid password');
        // }
        $this->pass = $pass;
    }

    // Will add dashes if have time ////////////////////////
    public function setPhone($phone) {
        if($phone=== NULL) {
            $this->phone = NULL;
            return;
          }
      
        if (!preg_match('/^[0-9]{10}$/', $phone)) {
            throw new Exception('Invalid phone');
        }
        $this->phone = $phone;
    }

    public function setEmail($email) {
        if($email === NULL) {
            $this->email = NULL;
            return;
        }

        if (!preg_match('/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/', $email)) {
            throw new Exception('Invalid email');
        }
        $this->email = $email;
    }

    public function setPassportNumber($passport_number) {
        if($passport_number === NULL) {
            $this->passport_number = NULL;
            return;
        }

        if (!preg_match('/^[0-9]{9}$/i', $passport_number)) {
            throw new Exception('Invalid passport number');
        }
        $this->passport_number = $passport_number;
    }

    public function save($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Member save');
        }

        if ($this->getId() === NULL) {

            $stt = $pdo->prepare('INSERT INTO members (username, pass, phone, email, passport_number) VALUES (:username, :pass, :phone, :email, :passport_number)');
            $stt->execute([
                'username' => $this->getUserName(),
                'pass' => $this->getPassword(),
                'phone' => $this->getPhone(),
                'email' => $this->getEmail(),
                'passport_number' => $this->getPassportNumber()
            ]);

            $inserted = $stt->rowCount() === 1;

            if ($inserted) {
                $this->setId($pdo->lastInsertId());
            }

            return $inserted;
        } else {

            $stt = $pdo->prepare('UPDATE members SET username = :username, pass = :pass, phone = :phone, email = :email, passport_number = :passport_number, WHERE id = :id LIMIT 1');
            $stt->execute([
                'id' => $this->getId(),
                'username' => $this->getUserName(),
                'pass' => $this->getPassword(),
                'phone' => $this->getPhone(),
                'email' => $this->getEmail(),
                'passport_number' => $this->getPassportNumber()
            ]);

            $updated = $stt->rowCount() === 1;

            return $updated;
        }
    }

    public function delete($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Member delete');
        }

        if ($this->getId() === NULL) {
            throw new Exception('Cannot delete a transient member');
        }

        $stt = $pdo->prepare('DELETE FROM members WHERE id = :id LIMIT 1');
        $stt->execute([
            'id' => $this->getId()
        ]);

        $deleted = $stt->rowCount() === 1;

        if ($deleted) {
            $this->setId(NULL);
        }

        return $deleted;
    }

    public static function findAllMembers($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Member findAllMembers');
        }

        $stt = $pdo->prepare('SELECT id, username, pass, phone, email, passport_number FROM members');
        $stt->execute();

        return array_map(function($row) {
            return new Member($row);
        }, $stt->fetchAll());
    }

    public static function findOneById($id, $pdo) {

        if (!Model::isValidId($id)) {
            throw new Exception('Invalid ID passed to setId for Member');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object for Member findOneById');
        }

        $stt = $pdo->prepare('SELECT id, username, pass, phone, email, passport_number FROM members WHERE id = :id LIMIT 1');
        $stt->execute([
            'id' => $id
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Member($row);
        }
    }

    public static function findOneByUsername($username, $pdo) {

        if (!Model::isValidId($username)) {
            throw new Exception('Invalid username passed to setUserName for Member');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object for Member findOneByUsername');
        }

        $stt = $pdo->prepare('SELECT id, username, pass, phone, email, passport_number FROM members WHERE username = :username LIMIT 1');
        $stt->execute([
            'username' => $username
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Member($row);
        }
    }

    public static function findOneByEmail($email, $pdo) {

        if (!Model::isValidId($email)) {
            throw new Exception('Invalid email passed to setEmail for Member');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object for Member findOneByEmail');
        }

        $stt = $pdo->prepare('SELECT id, username, pass, phone, email, passport_number FROM members WHERE email = :email LIMIT 1');
        $stt->execute([
            'email' => $email
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Member($row);
        }
    }

    public function doesMemberExist($pdo) {
        $stt = $pdo->prepare('Select id from members where username = :username');
        $stt->execute([
            'username' => 'username'
        ]);

        $stt->fetch();

        if ($stt->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function memberArray() {
        return[
            'id' => $this->getId(),
            'username' => $this->getUserName(),
            'pass' => $this->getPassword(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'passport_number' => $this->getPassportNumber()
        ];
    }
}
?>


