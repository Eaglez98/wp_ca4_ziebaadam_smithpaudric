<?php require_once ('Model.php'); ?>
<?php
use Rapid\Database;

class Destination {

    private $id;
    private $country;
    private $city;
    private $airport;
    private $time_zone;

    public function __construct($args) 
    {

        if (!is_array($args)) {
            throw new Exception('Destinations constructor requires an associative array');
        }

        $this->setId($args['id'] ?? NULL);
        $this->setCountry($args['country'] ?? 'Untitled destination');
        $this->setCity($args['city'] ?? 'Untitled city');
        $this->setAirport($args['airport'] ?? 'Untitled airport');
        // $this->setTime_Zone($args['time_zone'] ?? 'Untitled time zone');
    }

    //Getters
    public function getId() {
        return $this->id;
    }
    public function getCountry() {
        return $this->country;
    }
    public function getCity() {
        return $this->city;
    }
    public function getAirport() {
        return $this->airport;
    }
    public function getTimeZone() {
        return $this->time_zone;
    }

    // Setters
    public function setId($id) 
    {
        if ($id === NULL) 
        {
            $this->id = NULL;
            return;
        }
        if (!Model::isValidId($id)) 
        {
            throw new Exception('Invalid ID passed to setId for destination');
        }
        $this->id = $id;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setAirport($airport)
    {
        $this->airport = $airport;
    }

    public function setTimeZone($time_zone)
    {
        $this->time_zone = $time_zone;
    }


    public function save($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Destination save');
        }

        if ($this->getId() === NULL) {

            $stt = $pdo->prepare('INSERT INTO destinations (country, city, airport, time_zone) VALUES (:country, :city, :airport, :time_zone)');
            $stt->execute([
                'country' => $this->getCountry(),
                'city' => $this->getCity(),
                'airport' => $this->getAirport(),
                'time_zone' => $this->getTimeZone() 
            ]);

            $inserted = $stt->rowCount() === 1;

            if ($inserted) {
                $this->setId($pdo->lastInsertId());
            }

            return $inserted;
        } else {

            $stt = $pdo->prepare('UPDATE destinations SET country = :country, city = :city, airport = :airport, time_zone = :time_zone WHERE id = :id LIMIT 1');
            $stt->execute([
                'id' => $this->getId(),
                'country' => $this->getCountry(),
                'city' => $this->getCity(),
                'airport' => $this->getAirport(),
                'time_zone' => $this->getTimeZone
            ]);

            $updated = $stt->rowCount() === 1;

            return $updated;
        }
    }

    public function delete($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to destination delete');
        }

        if ($this->getId() === NULL) {
            throw new Exception('Cannot delete a transient destination');
        }

        $stt = $pdo->prepare('DELETE FROM destinations WHERE id = :id LIMIT 1');
        $stt->execute([
            'id' => $this->getId()
        ]);

        $deleted = $pdo->rowCount() === 1;

        if ($deleted) {
            $this->setId(NULL);
        }

        return $deleted;
    }

    public static function findAllDestinations($pdo) {
        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Destination findAllDestinations');
        }

        $stt = $pdo->prepare('SELECT id, country, city, airport, time_zone FROM destinations');
        $stt->execute();

        return array_map(function($row) {
            return new Destination($row);
        }, $stt->fetchAll());
    }

    public function findOneById($id, $pdo) {

        if (!Model::isValidId($id)) {
            throw new Exception('Invalid ID passed to setId for Destination');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Destination findAll');
        }

        $stt = $pdo->prepare('SELECT id, country, city, airport, time_zone FROM destinations WHERE id = :id');
        $stt->execute([
            'id' => $id
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Destination($row);
        }
        
    }

}


?>


