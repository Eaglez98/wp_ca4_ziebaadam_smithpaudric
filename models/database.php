<?php

$config = require 'config.php';

$dsn = 'mysql:host=' . $config['DATABASE_HOST'] . ';dbname=' . $config['DATABASE_NAME'] . ';charset=utf8mb4';
$db_username = $config['DATABASE_USER'];
$db_password = $config['DATABASE_PASS'];

try {
    $db = new PDO($dsn, $db_username, $db_password);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    error_reporting(E_ALL);
    return $db;
} catch (PDOExceptionException $ex) {
    $error_message = $ex->getMessage();
    include ('views/database_error.php');
    exit();
}



