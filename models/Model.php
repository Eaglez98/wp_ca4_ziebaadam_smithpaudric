<?php

class Model {

    public static function isValidId($id) {

        $id_is_bad = !is_numeric($id) || $id < 1;

        if ($id_is_bad) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
