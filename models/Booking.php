<?php require_once ('Model.php'); ?>
<?php
use Rapid\Database;

class Booking {

    private $id;
    private $time_stamp;
    private $flight_id;
    private $member_id;

    public function __construct($args) 
    {

        if (!is_array($args)) {
            throw new Exception('Booking constructor requires an associative array');
        }

        $this->setId($args['id'] ?? NULL);
        $this->setTimeStamp($args['time_stamp'] ?? 'Untitled time stamp');
        $this->setFlightId($args['flight_id'] ?? NULL);
        $this->setMemberId($args['member_id'] ?? NULL);
    }

    //Getters
    public function getId() {
        return $this->id;
    }
    public function getTimeStamp() {
        return $this->time_stamp;
    }
    public function getFlightId() {
        return $this->flight_id;
    }
    public function getMemberId() {
        return $this->member_id;
    }

    // Setters
    public function setId($id) 
    {
        if ($id === NULL) 
        {
            $this->id = NULL;
            return;
        }
        if (!Model::isValidId($id)) 
        {
            throw new Exception('Invalid ID passed to setId for booking');
        }
        $this->id = $id;
    }

    public function setTimeStamp($time_stamp) {
        $this->time_stamp = $time_stamp;
    }

    public function setFlight($flight_id)
    {
        $this->flight_id = $flight_id;
    }

    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    public function save($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Booking save');
        }

        if ($this->getId() === NULL) {

            $stt = $pdo->prepare('INSERT INTO bookings (time_stamp, flight_id, member_id) VALUES (:time_stamp, :flight_id, :member_id)');
            $stt->execute([
                'time_stamp' => $this->getTimeStamp(),
                'flight_id' => $this->getFlightId(),
                'member_id' => $this->getMemberId()
            ]);

            $inserted = $stt->rowCount() === 1;

            if ($inserted) {
                $this->setId($pdo->lastInsertId());
            }

            return $inserted;
        } else {

            $stt = $pdo->prepare('UPDATE bookings SET time_stamp = :time_stamp, flight_id = :fligt_id, member_id = :member_id, WHERE id = :id LIMIT 1');
            $stt->execute([
                'id' => $this->getId(),
                'time_stamp' => $this->getTimeStamp(),
                'flight_id' => $this->getFlightId(),
                'member_id' => $this->getMemberId()
            ]);

            $updated = $stt->rowCount() === 1;

            return $updated;
        }
    }

    public function delete($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to booking delete');
        }

        if ($this->getId() === NULL) {
            throw new Exception('Cannot delete a transient booking');
        }

        $stt = $pdo->prepare('DELETE FROM bookings WHERE id = :id LIMIT 1');
        $stt->execute([
            'id' => $this->getId()
        ]);

        $deleted = $pdo->rowCount() === 1;

        if ($deleted) {
            $this->setId(NULL);
        }

        return $deleted;
    }

    public static function findAll($pdo) {
        $statement = $pdo->prepare('SELECT id, time_stamp, flight_id, member_id FROM bookings');
        $statement->execute();

        $bookings = [];

        foreach ($statement->fetchAll() as $row) {
            array_push($bookings, new Booking($row));
        }
        return $bookings;
    }

    public function findOneById($id, $pdo) {

        if (!Model::isValidId($id)) {
            throw new Exception('Invalid ID passed to setId for Booking');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Booking findAll');
        }

        $stt = $pdo->prepare('SELECT id, time_stamp, flight_id, member_id FROM bookings WHERE id = :id');
        $stt->execute([
            'id' => $id
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Booking($row);
        }
        
    }

}