<?php require_once ('Model.php'); ?>
<?php
use Rapid\Database;

class Flight {

    private $id;
    private $destination_id;
    private $price;
    private $depart_time;
    private $arrive_time;
    private $arrivals_id;

    public function __construct($args) 
    {

        if (!is_array($args)) {
            throw new Exception('Flights constructor requires an associative array');
        }

        $this->setId($args['id'] ?? NULL);
        $this->setDestinationId($args['destination_id'] ?? NULL);
        $this->setPrice($args['price'] ?? NULL);
        $this->setDepartTime($args['depart_time'] ?? NULL);
        $this->setArriveTime($args['arrive_time'] ?? NULL);
        $this->setArrivalsId($args['arrivals_id'] ?? NULL);
    }

    //Getters
    public function getId() {
        return $this->id;
    }
    public function getDestinationId() {
        return $this->Destination_id;
    }
    public function getPrice() {
        return $this->price;
    }
    public function getDepartTime() {
        return $this->depart_time;
    }
    public function getArriveTime() {
        return $this->arrive_time;
    }
    public function getArrivalsId() {
        return $this->arrivals_id;
    }

    // Setters
    public function setId($id) 
    {
        if ($id === NULL) 
        {
            $this->id = NULL;
            return;
        }
        if (!Model::isValidId($id)) 
        {
            throw new Exception('Invalid ID passed to setId for flights');
        }
        $this->id = $id;
    }

    public function setDestinationId($destination_id)
    {
        $this->destination = $destination_id;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setDepartTime($depart_time)
    {
        $this->depart_time = $depart_time;
    }

    public function setArriveTime($arrive_time)
    {
        $this->arrive_time = $arrive_time;
    }

    public function setArrivalsId($arrivals_id)
    {
        $this->arrivals_id = $arrivals_id;
    }


    public function save($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Flight save');
        }

        if ($this->getId() === NULL) {

            $stt = $pdo->prepare('INSERT INTO flights (destination_id, price, depart_time, arrive_time, arrivals_id) VALUES (:destination_id, :price, :depart_time, :arrive_time, :arrivals_id)');
            $stt->execute([
                'destination_id' => $this->getDestinationId(),
                'price' => $this->getPrice(),
                'depart_time' => $this->getDepartTime(),
                'arrive_time' => $this->getArriveTime(),
                'arrivals_id' => $this->getArrivalsId()
            ]);

            $inserted = $stt->rowCount() === 1;

            if ($inserted) {
                $this->setId($pdo->lastInsertId());
            }

            return $inserted;
        } else {

            $stt = $pdo->prepare('UPDATE flights SET destination_id = :destination_id, price = :price, depart_time = :depart_time, arrive_time = :arrive_time, arrivals_id = :arrivals_id WHERE id = :id LIMIT 1');
            $stt->execute([
                'id' => $this->getId(),
                'destination_id' => $this->getDestinationId(),
                'price' => $this->getPrice(),
                'depart_time' => $this->getDepartTime(),
                'arrive_time' => $this->getArriveTime(),
                'arrivals_id' => $this->getArrivalsId()
            ]);

            $updated = $stt->rowCount() === 1;

            return $updated;
        }
    }

    public function delete($pdo) {

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to flight delete');
        }

        if ($this->getId() === NULL) {
            throw new Exception('Cannot delete a transient flight');
        }

        $stt = $pdo->prepare('DELETE FROM flights WHERE id = :id LIMIT 1');
        $stt->execute([
            'id' => $this->getId()
        ]);

        $deleted = $pdo->rowCount() === 1;

        if ($deleted) {
            $this->setId(NULL);
        }

        return $deleted;
    }

    public static function findAll($pdo) {
        $statement = $pdo->prepare('SELECT id, destination_id, price, depart_time, arrive_time, arrivals_id FROM flights');
        $statement->execute();

        $flights = [];

        foreach ($statement->fetchAll() as $row) {
            array_push($flights, new Flight($row));
        }
        return $flights;
    }

    public function findOneById($id, $pdo) {

        if (!Model::isValidId($id)) {
            throw new Exception('Invalid ID passed to setId for Flight');
        }

        if (!($pdo instanceof PDO)) {
            throw new Exception('Invalid PDO object given to Flight findAll');
        }

        $stt = $pdo->prepare('SELECT id, destination_id, price, depart_time, arrive_time, arrivals_id FROM flights WHERE id = :id');
        $stt->execute([
            'id' => $id
        ]);

        $row = $stt->fetch();

        if ($row === FALSE) {
            return NULL;
        } else {
            return new Flight($row);
        }
        
    }

    
}
?>