# #ExperienceTravel
### An exciting european travel website like no other! :)  

We have decided to do a #ExperienceTravel booking website as we are both passionate about travelling. Working on this project from the planning to actually executing queries and final polish gave us a deep
understanding of how a booking website works and what has to be implemented and thought-through in order to deliver a professional user-focused experience which would possibly attract future customers.

Working on this project greatly challenged us and we have learned a lot as we progressed from stage to stage, facing many problems and coming up with compromises along the way I think we've managed to create a decent website. We had a lot of fun working on `#ExperienceTravel`!!!


# Development Process
- [Idea Generation] *#ExperienceTravel*, *booking_website*, *travel*, *user_admin*, *login*, *europe*
  - [Entity-Relationship](\assets\screenshots\initialDatabaseTables.JPG)
  - [Populating necessary tables e.g flights](\assets\screenshots\flightsTable.JPG)
- [After tables have been completed we worked on *BootStrap* template for every page]
- [In the mean-time we designed *Models* for each Entity table and overloaded some of them.]
- [Along with Models we've done *PDOs* & created initial statements for *Controllers*]
- [Then we worked on bootstrap *template* adding in different functions and changing the font: `https://fonts.google.com/specimen/Indie+Flower?selection.family=Indie+Flower`]
- []


# Things we didn't get working
- We didn't manage to get *$city* to be extracted from the db and shown in the **home.php* view,
- For some reason the panorma.jpg / panorama2.jpg didn't load in the jumbotron component

# If we had more time..
- We would certainly improve the bootstrap content and AJAX for different views to make the website even more apealing,
- we would manage to get all the routes directed correctly,
- all the buttons to execute the right re-direct action,


