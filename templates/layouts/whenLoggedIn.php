<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <title> <?= $locals["pageTitle"]; ?> </title> -->

        <!-- Bootstrap css -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
              crossorigin="anonymous">

        <!-- Bootstrap Dependencies -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                crossorigin="anonymous"></script>

        <!-- Bootstrap JS -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                crossorigin="anonymous"></script>

        <!-- Google Font -->
        <style href="https://fonts.googleapis.com/css?family=Indie+Flower"></style>
        
        <!--Style CSS-->
        <link href="<?=APP_BASE_URL?>/assets/styles/style.css" rel="stylesheet">

    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" data-interval="3500">
                <img src="assets\site_images\dublin.jpg" class="d-block w-100" alt="First Slide">
                <div class="carousel-caption d-none d-md-block">
                    <div class="country-title-dub">
                    <div class="carousel-title">#ExperienceTravel</div>
                        <h1>Dublin</h1>
                        <h2><p>Dia dhuit!</p></h2>
                    </div>
                </div>
            </div>
        <div class="carousel-item" data-interval="3500">
            <img src="assets\site_images\berlin1.jpg" class="d-block w-100" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="country-title-ber">
                <div class="carousel-title">#ExperienceTravel</div>
                    <h1>Berlin</h1>
                    <h2><p>Guten Tag!</p></h2>
                </div>
            </div>
        </div>
        <div class="carousel-item" data-interval="3500">
            <img src="assets\site_images\london1.jpg" class="d-block w-100" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="country-title-lon">
                <div class="carousel-title">#ExperienceTravel</div>
                    <h1>London</h1>
                    <h2><p>Hello!</p></h2>
                </div>
            </div>
        </div>
        <div class="carousel-item" data-interval="3500">
            <img src="assets\site_images\madrid1.jpg" class="d-block w-100" alt="Fourth slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="country-title-mad"> 
                <div class="carousel-title">#ExperienceTravel</div>
                    <h1>Madrid</h1>
                    <h2><p>Hola!</p></h2>
                </div>
            </div>
        </div>
        <div class="carousel-item" data-interval="3500">
            <img src="assets\site_images\paris1.jpg" class="d-block w-100" alt="Fifth slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="country-title-par">
                    <div class="carousel-title">#ExperienceTravel</div>
                    <h1>Paris</h1>
                    <h2><p>Bonjour!</p></h2>
                </div>
            </div>
        </div>


        <!-- Carousel prev/next slide indicatiors -->
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>


        <!-- NAVBAR TOP -->
        <nav class="navbar navbar-expand-sm fixed-top navbar-light">
            <div class="container">
            <div class="navigation-options" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="navbar-home" href="<?=APP_BASE_URL?>/" style="color: #000000">Home</a>
                    </li>
                    <li class ="nav-item">
                        <a class="navbar-destinations" href="<?=APP_BASE_URL?>/destinations" style="color: #000000">Destinations</a>
                    </li>
                    <li class ="nav-item">
                        <a class="navbar-experiences" href="<?=APP_BASE_URL?>/experiences" style="color: #000000">Experiences</a>
                    </li>
                    <li class ="nav-item">
                        <a class="navbar-logout" href="<?=APP_BASE_URL?>/logout" style="color: #000000">LogOut</a>
                    </li>
                    <li class ="nav-item">
                        <a class="navbar-about" href="<?=APP_BASE_URL?>/my_bookings" style="color: #000000">My bookings</a>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
    </div>
    </head>


    <body>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Fluid jumbotron</h1>
                <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
            </div>
        </div>

            
            <?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>
        </div>
    </body>

    <!-- Footer -->
    <footer>
    <div class="container-fluid padding">
        <div class="row text-center">
            <div class="col-md-4">
                <hr class="light">
                <h5>Contact us</h5>
                <hr class="light">
                <p>+353 99 999 9999</p>
                <p>email@experiencetravel.com</p>
                <p>21 Jump Street</p>
                <p>Dundalk, Co. Meath</p>
            </div>
        <div class="col-md-4">
            <hr class="light">
            <h5>About Us</h5>
            <hr class="light">
            <p>A small team of travelling<p>
            <p>freaks from Ireland providing<p>
            <p>cheap and best flights!<p>
        </div>
        <div class="col-md-4">
            <hr class="light">
            <h5>Offices</h5>
            <hr class="light">
            <p> 12 O'Connel St. Dublin Ireland<p> 
            <p> 113 Kopernicken Strasse Berlin Germany<p>
            <p> 76 Capri Ld Paris France<p>
        </div>
        <div class="col-12">
            <hr class="light">
            <h5>&copy; Paudric Smith & Adam Zieba All Rights Reserved </h5>
        </div>
        </div>
    </footer>
</html>