<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Database Error</title>
        <link href="assets/styles/stylesheet.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Database Error</h1>
            <p>There has been a database error: <?php echo $error_message; ?></p>
        </main>
        
    </body>
</html>

