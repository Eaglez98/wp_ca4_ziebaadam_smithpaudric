<table class="table">
    <thead class='thead-dark'>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Passport No.</th>
        </tr>
    </thead>
    <tbody>
        <tr><?php foreach ($locals['findAllMembers'] as $member) : ?></tr>
            <td><?= $member->getUserName(); ?></td>
            <td><?= $member->getEmail(); ?></td>
            <td><?= $member->getPhone(); ?></td>
            <td><?= $member->getPassportNumber(); ?></td>
            <td><a href="<?= APP_BASE_URL ?>/delete_member_process?id=<?= $member->getId()?>">
               <button type="submit" name="delete_member" class="btn">
                    Delete
                </button>           
            <td><a href="<?= APP_BASE_URL ?>/Update_member?id=<?= $member->getId() ?>">
                <button type="submit" name="update_member" class="btn">
                    Update
                </button>           
        <?php endforeach; ?>

    </tbody>

</table>