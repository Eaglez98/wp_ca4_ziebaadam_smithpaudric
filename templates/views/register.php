<div class="container">
    <?php if(isset($locals['error'])) { ?>
        <div class="alert alert-danger">
            <strong>Oops, something went wrong :(</strong>
            <p><?=$locals['error']?></p>
        </div>
    <?php }?>
<div class="card bg-light">
    <article class="card-body mx-auto" >
        <h2 class="card-title mt-3 text-center">Create Account</h2>
        <p class="text-center">and book the experience of your life!</p>
        <form action="register" method='post'>
        <!-- username -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope">U</i> </span>
                </div>
                <input name="username" class="form-control" placeholder="Username" type="text">
            </div>
        <!-- email -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope">@</i> </span>
                </div>
                <input name="email" class="form-control" placeholder="Email address" type="email">
            </div> 
        <!-- password -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-lock">?</i> </span>
                </div>
                <input name="password" class="form-control" placeholder="Password" type="password">
            </div>  
        <!-- phone_number -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-lock">#</i> </span>
                </div>
                <input name="phone_number" class="form-control" placeholder="Phone number" type="number">
            </div>
        <!-- passport number -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope">o</i> </span>
                </div>
                <input name="passport_number" class="form-control" placeholder="Passport number" type="number">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
            </div>  
            <p class="text-center">Have an account? <a href="<?=APP_BASE_URL?>/login">Log In</a> </p>
        </form>
    </article> 
</div>  