<div class="jumbotron jumbotron-fluid">
    <div class="container">
    <div class="row">
        <div id="where-happening">
            <p><h2> So where is the experience happening?</h2></p>
        </div>
        <select class="custom-select">
            <option selected>Travelling From?</option>
            <?php foreach($locals['destinations'] as $destination) {?>

            <option value="1"><?= $destination->getCity() ?></option>
            <option value="2">Dublin</option>
            <option value="3">Berlin</option>
            <option value="4">London</option>
            <option value="5">Madrid</option>
            <option value="6">Paris</option>
            <?php }?>
        </select>
        <br>
        <select class="custom-select">
            <option selected>To experience Where?</option>
            <?php foreach($locals['destinations'] as $destination) {?>

            <option value="1"><?= $destination->getCity() ?></option>
            <option value="2">Dublin</option>
            <option value="3">Berlin</option>
            <option value="4">London</option>
            <option value="5">Madrid</option>
            <option value="6">Paris</option>
            <?php }?>
        </select>
    </div>
        <p>
        <button id='lets-button' type="button" class="btn4 btn-primary btn-lg">Let's Go!</button>
    </div>
</div>

<h2>Destinations</h2>
    <div class="row" style="margin-top: 20px;">
    <?php foreach($locals['destinations'] as $destination) { ?>
        <div class="col-sm-3">
        <div class="card">
        <div class="card-body">
        <h5 class="card-title">City: <?= $destination->getCity() ?></h5>
        <p class="card-text">Country: <?= $destination->getCountry() ?></p>
        <p class="card-text">Country: <?= $destination->getAirport() ?></p>
        <div class="card-footer">
        <a class='btn btn-success btn-xs' href="update_destination?user_id=<?= $destination->getId() ?>"> Edit</a>
        <a class="btn btn-danger btn-xs" href="delete_destination?user_id=<?= $destination->getId() ?>">Delete</a>
        </div>
        </div>
        </div>
        </div>
  <?php }?>
  </div>