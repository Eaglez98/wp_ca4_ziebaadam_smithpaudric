<?php

return function($req, $res) {
    $req->sessionStart();

    require('./models/Member.php');

    $pdo = \Rapid\Database::getPDO();

    $newMember = new Member([
        'username' => $req->body('username'),
        'pass' => $req->body('password'),
        'phone' => $req->body('phone'),
        'email' => $req->body('email'),
        'passport_number' => $req->body('passport_number')
    ]);

    if ($newMember->save($pdo)) {
        $res->redirect('/?success=1');
    }

    $res->redirect('/add_member?success=0');
}?>