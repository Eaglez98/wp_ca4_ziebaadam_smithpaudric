<?php return function($req, $res) {

$req->sessionStart();

require('./models/Destination.php');

$pdo = \Rapid\Database::getPDO();


$destination = Destination::findAllDestinations($pdo);

$res->render('main', 'home',[
  'pageTitle' => 'Home',
  'destinations' => $destination
]);

if(!$req->session('LOGGED_IN'))
{
  $res->render('main', 'home',[
    'pageTitle'=>'Home'
  ]);
}else{
    $res->render('whenLoggedIn', 'home', [
        'pageTitle' => 'Home'
    ]);
}

} 

?>
