<?php

return function($req, $res) {

    $res->render('main', 'destinations', [
        'pageTitle' => 'Destinations'
    ]);
}
?>