<?php

return function($req, $res) {
    $req->sessionStart();

    require('./models/Member.php');

    $config = \Rapid\ConfigFile::getContent();
    $pdo = \Rapid\Database::getPDO();

    $members = Member::findAllMembers($pdo);

    $res->render('whenLoggedInBlank', 'add_member', [
        'pageTitle' => 'Add_member',
        'members' => $members
    ]);
}
?>
