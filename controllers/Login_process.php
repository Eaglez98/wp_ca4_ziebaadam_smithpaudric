<?php

return function($req, $res) {
    $req->sessionStart();

    require_once('./models/Member.php');
    $pdo = \Rapid\Database::getPDO();

    $email = $req->body('email');
    echo($email);
    $password = $req->body('password');
    echo($password);

    $signIn = new Member([
        'email' => $email
    ]);

    if (empty($email) || empty($password)) {
        $res->render('blank', 'login', [
            'pageTitle' => 'Login',
            'error' => 'At least one field is empty'
        ]);
    } elseif (!$signIn->doesMemberExist($pdo)) {

        $res->render('blank', 'login', [
            'pageTitle' => 'Login',
            'error' => 'That username could not be found'
        ]);
    } else {
        $tempMember = $signIn->findOneByEmail($email, $pdo);
        if (!password_verify($password, $tempMember->getPassword())) {
            $res->render('whenLoggedOutBlank', 'login', [
                'pageTitle' => 'Login',
                'error' => 'Not correct Login details'
            ]);
        } else {
            $req->sessionSet('id' , $tempMember->getId());
            $req->sessionSet('username', $tempMember->getUserName());
            $req->sessionSet('pass', $tempMember->getPassword());
            $req->sessionSet('email', $tempMember->getEmail());
            $req->sessionSet('phone', $tempMember->getPhone());
            $req->sessionSet('passport_number', $tempMember->getPassportNumber());
            $req->sessionSet('LOGGED_IN', TRUE);
            $res->redirect('/');
        }
    }
}
?>


