<?php return function($req, $res) {

$req->sessionStart();

require('./models/Member.php');

$pdo = \Rapid\Database::getPDO();

$id = $req->query('id');

if(isset($id))
{
    $deleteMember = Member::findOneById($id, $pdo);
    $deleteMember->delete($pdo);
}
$res->redirect("/update_delete_member?delete=1");
} 

?>