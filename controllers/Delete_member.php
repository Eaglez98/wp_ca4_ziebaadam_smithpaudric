<?php
return function ($req, $res) {

    $req->sessionStart();

    require('./models/Member.php');

    $pdo = \Rapid\Database::getPDO();

    $members = Member::findAllMembers($pdo);

    $res->render('whenLoggedInBlank', 'update_delete_member', [
        'pageTitle' => 'Delete_member',
        'findAllMembers' => $members
    ]);
}
?>