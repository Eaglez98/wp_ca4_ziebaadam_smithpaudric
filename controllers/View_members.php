<?php

return function($req, $res) {
    require('./models/Member.php');

    $req->sessionStart();
    
    $pdo = \Rapid\Database::getPDO();
    
    $members = Member::findAllMembers($pdo);

    $res->render('whenLoggedInBlank', 'view_members', [
        'pageTitle' => 'View_Members',
        'members' => $members
    ]);
}

?>
