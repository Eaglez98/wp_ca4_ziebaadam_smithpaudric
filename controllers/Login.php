<?php

return function ($req, $res) {
    $req->sessionStart();

    if (!$req->session('LOGGED_IN')) {
        $res->render('whenLoggedOutBlank', 'login', [
            'pageTitle' => 'Login'
        ]);
    } else {
        $res->render('whenLoggedIn', 'home', [
            'pageTitle' => 'Home'
        ]);
    }
}
?>