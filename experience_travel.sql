-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2019 at 09:45 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `experience_travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `time_stamp` datetime NOT NULL,
  `flight_id` bigint(255) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `airport` varchar(50) NOT NULL,
  `time_zone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`id`, `country`, `city`, `airport`, `time_zone`) VALUES
(1, 'Ireland', 'Dublin', 'DUB', 'UTC + 0'),
(2, 'England', 'London', 'LHR', 'UTC + 0'),
(3, 'France', 'Paris', 'CDG', 'UTC + 1'),
(4, 'Spain', 'Madrid', 'MAD', 'UTC + 1'),
(5, 'Germany', 'Berlin', 'SXF', 'UTC + 1');

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE `flights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `destination_id` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `depart_time` datetime NOT NULL,
  `arrive_time` datetime NOT NULL,
  `arrivals_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`id`, `destination_id`, `price`, `depart_time`, `arrive_time`, `arrivals_id`) VALUES
(1, 1, 79.99, '2019-05-15 15:45:00', '2019-05-15 16:30:00', 2),
(2, 1, 119.99, '2019-05-16 18:00:00', '2019-05-16 20:00:00', 3),
(3, 1, 129, '2019-05-16 07:30:00', '2019-05-16 09:00:00', 4),
(4, 1, 109.99, '2019-05-17 12:00:00', '2019-05-17 14:00:00', 5),
(5, 2, 78.54, '2019-05-19 01:00:00', '2019-05-19 01:30:00', 1),
(6, 2, 129, '2019-05-21 11:00:00', '2019-05-21 11:40:00', 3),
(7, 3, 159, '2019-05-23 17:00:00', '2019-05-23 18:10:00', 1),
(8, 3, 167.89, '2019-05-29 08:40:00', '2019-05-29 09:20:00', 5),
(9, 4, 189, '2019-05-30 13:00:00', '2019-05-30 14:20:00', 1),
(10, 5, 217.98, '2019-05-27 00:00:00', '2019-05-27 02:20:00', 1),
(11, 2, 69.87, '2019-06-05 00:00:00', '2019-06-05 02:00:00', 4),
(12, 2, 123.23, '2019-05-23 11:00:00', '2019-05-23 13:00:00', 5),
(13, 3, 187.23, '2019-05-27 00:00:00', '2019-05-27 02:00:00', 4),
(14, 3, 198, '2019-06-05 10:00:00', '2019-06-05 10:00:00', 2),
(15, 4, 86.66, '2019-05-11 20:00:00', '2019-05-11 21:00:00', 2),
(16, 4, 119, '2019-06-21 05:00:00', '2019-06-21 06:00:00', 3),
(17, 4, 56.77, '2019-05-08 14:00:00', '2019-05-08 15:00:00', 5),
(18, 5, 123.56, '2019-05-01 00:00:00', '2019-05-01 01:00:00', 2),
(19, 5, 15.99, '2019-05-11 10:00:00', '2019-05-11 11:00:00', 3),
(20, 5, 99.99, '2019-05-18 07:00:00', '2019-05-18 08:00:00', 4);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passport_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `password`, `phone`, `email`, `passport_number`) VALUES
(1, 'Paudric Smith', 'Password01', '0123456789', 'paudricsmith@gmail.com', '012345678'),
(2, 'Mr Bean', 'Password02', '0861234567', 'mrbean@yahoo.com', '222222222'),
(3, 'Donald Trump', 'Password03', '0863333333', 'donaldtrump@hotmail.com', '333333333'),
(4, 'Clint Eastwood', 'Password04', '0864444444', 'clinteastwood@gmail.ie', '444444444'),
(5, 'Big Lez', 'Password05', '0865555555', 'biglez@yahoo.ie', '555555555');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flight_id` (`flight_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destination_id` (`destination_id`),
  ADD KEY `arrivals_id` (`arrivals_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`);

--
-- Constraints for table `flights`
--
ALTER TABLE `flights`
  ADD CONSTRAINT `flights_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`),
  ADD CONSTRAINT `flights_ibfk_2` FOREIGN KEY (`arrivals_id`) REFERENCES `destinations` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
