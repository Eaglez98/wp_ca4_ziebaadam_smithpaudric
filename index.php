<?php
  use Rapid\ConfigFile;

  // Include the Rapid library
  require_once('lib/Rapid.php');
  $config = \Rapid\ConfigFile::getContent();
  DEFINE('APP_BASE_URL', ConfigFile::getContent()['ENV']);

  // Create a new Router instance
  $app = new \Rapid\Router();

  // Define some routes. Here: requests to / will be       
  // processed by the controller at controllers/Home.php
  $app->GET('/', 'Home');
  $app->GET('/about', 'About');
  $app->GET('/destinations', 'Destinations');
  $app->GET('/experiences', 'Experiences');
  $app->GET('/register', 'Register');
  $app->GET('/login', 'Login');
  $app->POST('/login', 'Login_process');
  $app->GET('/logout', 'Logout');
  $app->GET('/my_bookings', 'My_bookings');
  $app->GET('/view_members', 'View_members');
  $app->GET('/add_member', 'Add_member');
  $app->POST('/add_member', 'Add_member_process');
  $app->GET('/update_delete_member', 'Delete_member');
  $app->GET('/delete_member_process', 'Delete_member_process');
  $app->GET('/update_member', 'Member_update');
  $app->POST('/update_member', 'Member_update_process');

  // Process the request
  $app->dispatch();

?>